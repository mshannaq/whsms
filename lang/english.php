<?php
//alignments, to not translate just choose best value
$_ADDONLANG['basealign'] = "left"; //for LTR languages the basealign is left , and for RTL languages the basealign is right

//translate here
$_ADDONLANG['introtext'] = "This addon allow you to use sms gateway to send sms from whmcs";
$_ADDONLANG['description']  = "This addon allow you to use  sms gateway to send sms fron whmcs";
$_ADDONLANG['documentation'] = "at this stage from the software age there is no documentation for this addon. contact the developer for more help.";
$_ADDONLANG['notconfiguredyer'] = "The WHSMS addon did not configured yet or misconfigured. please configure the addon from Setup > Addon modules > WHSMS > configure button and make sure from choosing the sms gateway and the username and password entered correctly.";
$_ADDONLANG['notworkinguntilthat'] = "Until that the addon and the SMS functionality will not work correctly";
$_ADDONLANG['sendsinglesms'] = "Send Single SMS";
$_ADDONLANG['smsmessage'] = "SMS Message";
$_ADDONLANG['from'] = "From";
$_ADDONLANG['tonumber'] = "To number";
$_ADDONLANG['character_count'] = "Character count";
$_ADDONLANG['message_points_count'] = "Message Points count";

$_ADDONLANG['youmustenterallformfileds'] = "You must fill all form fields, double check your entry";

$_ADDONLANG['messagenotsent'] = "Message NOT sent";
$_ADDONLANG['goback'] = "Go Back";
$_ADDONLANG['sendsmsbtn'] = "Send SMS";


$_ADDONLANG['send_sms_error'] = "SMS sending error";
$_ADDONLANG['message_sent'] = "Message sent successfully";
$_ADDONLANG['cannot_connect'] = "Cannot connct to SMS gateway";
$_ADDONLANG['balance_empty'] = "SMS gateway balace empty";
$_ADDONLANG['balance_not_enough'] = "Your credit at SMS gateway is not enough to send this message";
$_ADDONLANG['wrong_username'] = "SMS gateway username is wrong";
$_ADDONLANG['worng_password'] = "SMS gateway password is worng";
$_ADDONLANG['request_page_temprary_not_av'] = "SMS request destination is not available temporary, try again later";
$_ADDONLANG['trial_expired'] = "SMS gateway trial period expired";
$_ADDONLANG['numbers_no_notequal_messages'] = "numbers No. not match the messages No.";
$_ADDONLANG['unaccebtable_sender'] = "The used sender 'from' is not accepted by the gateway";
$_ADDONLANG['inactive_sender'] = "The used sender 'from' is inactive, you must activate it at the gateway before sending";
$_ADDONLANG['error_to_numbers']="one or more from the recipients numbers is invalid";
$_ADDONLANG['null_sender_or_error']="sender name 'from' is empty or invalid";
$_ADDONLANG['msg_not_encoded_correctly_or_not_entered']="The entered message is empty or not encoded correctly";
$_ADDONLANG['unknown_error'] = "Unknown Error!";




