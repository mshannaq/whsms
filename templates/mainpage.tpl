<script src="../modules/addons/whsms/js/whsms.js"></script>
<p>
    {$LANG.introtext} 
</p>
<p>
    SMS gateway balance: {$whsms_smsgateway_balance}
</p>

<h2>{$LANG.sendsinglesms}</h2>
<form action="{$modulelink}&action=sendsms" method="post">
    <div>
        <div style="float:{$LANG.basealign};width: 100px;"><label for="from">{$LANG.from}:</label></div>
        <div style="float:{$LANG.basealign};">
            <select name="from" id="from">
                {foreach from=$gatewaysenders item=sender}
                    <option value="{$sender}" {if $sender eq $gatewaydefaultsender} selected {/if} >{$sender}</option>
                {/foreach}
            </select>

        </div>


    </div>
    <div style="clear: both;padding-top: 5px;"></div>
    <div>
        <div style="float:{$LANG.basealign};width: 100px;"><label for="to">{$LANG.tonumber}:</label></div>
        <div style="float:{$LANG.basealign};"><input type="text" name="to" id="to" size="20" /></div>


    </div>
    <div style="clear: both;padding-top: 5px;"></div>
    <div>
        <div style="float:{$LANG.basealign};width: 100px;"><label for="smsmsg">{$LANG.smsmessage}:</label></div>
        <div style="float:{$LANG.basealign};">
            <textarea rows="6" cols="40" name="smsmsg" id="smsmsg"></textarea>
        </div>
    </div>
    <div style="clear: both;padding-top: 5px;"></div>
    <div style="float:{$LANG.basealign};width: 100px;"> </div>    
    <div style="float:{$LANG.basealign};">{$LANG.character_count}: <span id="charcount" data-count=0>0</span></div>
    <div style="clear: both;padding-top: 5px;"></div>
    <div style="float:{$LANG.basealign};width: 100px;"> </div>    
    <div style="float:{$LANG.basealign};">{$LANG.message_points_count}: <span id="pointscount" data-count=1>1</span></div>

    <div style="clear: both;padding-top: 15px;"></div>
    <div>
        <div style="float:{$LANG.basealign};width: 100px;"> <input type="submit" value="{$LANG.sendsmsbtn}" />  </div>

    </div>


</form>