<?php

/*
 * whsms.php
 * WHSMS  WHMCS addon that let you send sms from WHCMS.
 * @copyright 2015 Mohammed AlShannaq http://shannaq.com
 * @author mshannaq
 * @link  http://shannaq.com
 * @version 1.0
 * @package whsms
 * 
 * 
 * 
 * The addon code is protected by copyright law, using this addon needs a license 
 * To get your license visit http://shannaq.com/l/whsms
 * support the developer and the addon to have a future update by buying a license.
 *  
 * current version support mobily.ws sms gateway only, next version may include other gateways
 * 
 */
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");
define(whsms_ppath, dirname(__FILE__));

function whsms_config() {
    $configarray = array(
        "name" => "WHSMS",
        "description" => "This addon allow you to use sms gateway to send sms from whmcs",
        "version" => "1.0",
        "author" => "mshannaq",
        "language" => "english",
        "fields" => array(
            "smsgateway" => array("FriendlyName" => "SMS Gateway", "Type" => "dropdown", "Options" =>
                "mobily.ws", "Description" => "Choose the SMS gateway", "Default" => "mobily.ws",),
            "gatewayusername" => array("FriendlyName" => "SMS gateway username", "Type" => "text", "Size" => "25",
                "Description" => "Enter your username",),
            "gatewaypassword" => array("FriendlyName" => "MS gateway password", "Type" => "password", "Size" => "25",
                "Description" => "Enter the gateway password",),
            "gatewaysenders" => array("FriendlyName" => "Allowed Sender names", "Type" => "textarea", "Size" => "25",
                "Description" => "Each sender in new line, atleast one sender must entered",),
            "gatewaydefaultsender" => array("FriendlyName" => "Default Sender", "Type" => "text", "Size" => "25",
                "Description" => "Enter default sender name or number",),
        )
    );
    return $configarray;
}

function whsms_activate() {

    

    # Return Result
    return array('status' => 'success', 'description' => 'whsms addon successful activated, configure the add on now to use it.');
    return array('status' => 'error', 'description' => 'error while activating whsms addon');
    
}

function whsms_deactivate() {
    
    # Return Result
    return array('status' => 'success', 'description' => 'whsms addon successful deactivated and sent log table dropped');
    return array('status' => 'error', 'description' => 'Error while deactivating whsms addon');
    
}

function whsms_output($vars) {

    $modulelink = $vars['modulelink'];
    $version = $vars['version'];
    $smsgateway = $vars['smsgateway'];
    $gatewayusername = $vars['gatewayusername'];
    $gatewaypassword = $vars['gatewaypassword'];
    $gatewaysenders = $vars['gatewaysenders'];
    $gatewaydefaultsender = $vars['gatewaydefaultsender'];
    $LANG = $vars['_lang'];


    if ($gatewayusername == "" || $gatewaypassword == "" || $gatewaysenders == "" || $gatewaydefaultsender == "") {
        whsms_error($LANG['notconfiguredyer'] . " " . $LANG['notworkinguntilthat']);
    } else {



        switch ($smsgateway) {
            case 'mobily.ws':
                require_once whsms_ppath . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'mobilyws' . DIRECTORY_SEPARATOR . 'mobilyws.class.php';
                $mobilyws = new mobilyws($gatewayusername, $gatewaypassword);
                break;
            default:
                break;
        }

        require_once whsms_ppath . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'sms_functions.php';


        //template engine
        $smarty = new Smarty();
        $smarty->caching = false;
        $smarty->compile_dir = $GLOBALS['templates_compiledir'];
        //assign language 
        $smarty->assign("LANG", $LANG);
        //global assign
        $smarty->assign("modulelink", $modulelink);        
        $smarty->assign("gatewaysenders",  explode("\n",$gatewaysenders));
        
        $smarty->assign("gatewaydefaultsender",$gatewaydefaultsender);



        switch ($_REQUEST['action']) {
            case 'sendsms':                
                //check gevien varibales
                if ($_POST['to'] == "" || $_POST['smsmsg'] == "") {
                    whsms_error($LANG['youmustenterallformfileds'] . ". " . $LANG['messagenotsent'] .  ". <a href='javascript:window.history.back()'>{$LANG['goback']}</a>");                    
                } else {
                    //try to send                                        
                    $send_res = whsms_send_sms($smsgateway,$mobilyws,trim($_POST['from']),$_POST['to'],$_POST['smsmsg']);                    
                    if ($send_res['result'] != "1"){
                        //error while sending
                        whsms_error($LANG['send_sms_error'] . ": " . $LANG[$send_res['text']]);
                    } else {
                        //message send
                        whsms_sent($LANG[$send_res['text']]);
                        
                    }
                    
                    
                }

                break;

            default:
                //getting the gateway balance
                $whsms_smsgateway_balance = whsms_get_balance($smsgateway, $mobilyws);
                $smarty->assign("whsms_smsgateway_balance", $whsms_smsgateway_balance);

                //display main page of addon
                $smarty->display(whsms_ppath . '/templates/mainpage.tpl');
                break;
        }
        
        echo "<div><br /><br /><br>WHSMS Addon <a href='http://shannaq.com/l/whsms/?v=1.0' target=_blank>v1.0</a></div>";
        
    }
}

function whsms_error($errortext, $type = "warning") {
    echo "<div style='font-size:130%;font-weight:bold;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;background-color: rgb(255, 255, 210);'>" . $errortext . ".</div>";
}

function whsms_sent($text){
    echo "<div style='font-size:130%;font-weight:bold;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;background-color: green;'>" . $text . ".</div>";
}