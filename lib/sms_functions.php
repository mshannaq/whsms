<?php

/*
 * sms_functions.php
 * WHSMS  WHMCS addon that let you send sms from WHCMS.
 * @copyright 2015 Mohammed AlShannaq http://shannaq.com
 * @author mshannaq
 * @link  http://shannaq.com
 * @version 1.0
 * @package whsms
 *  
 * 
 * 
 * 
 * The addon code is protected by copyright law, using this addon needs a license 
 * To get your license visit http://shannaq.com/l/whsms
 * support the developer and the addon to have a future update by buying a license.
 *  
 * current version support mobily.ws sms gateway only, next version may include other gateways
 * 
 */
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

function whsms_get_balance($gateway, $gateway_class_name) {

    switch ($gateway) {
        case 'mobily.ws':
            $balance_array = $gateway_class_name->get_balance();
            if ($balance_array['connected']) {
                //203/85
                $balanc_pieces = explode('/', $balance_array['result']);
                $net_balance = $balanc_pieces[1];
                return $net_balance;
            } else {
                return false;
            }

            break;

        default:
            return false;
            break;
    }
}

function whsms_send_sms($gateway, $gateway_class_name, $from, $to, $msg) {
    switch ($gateway) {
        case 'mobily.ws':
            $send_sms_res = $gateway_class_name->send_sms($from, $to, $msg);
            $res = array();
            $res['result'] = $send_sms_res;              
            switch ($send_sms_res) {
                case '0':
                    $res_text = "cannot_connect";
                    break;
                case '1':
                    $res_text = "message_sent";
                    break;
                case '2':
                    $res_text = "balance_empty";
                    break;
                case '3':
                    $res_text = "balance_not_enough";
                    break;
                case '4':
                    $res_text = "wrong_username";
                    break;
                case '5':
                    $res_text = "worng_password";
                    break;
                case '6':
                    $res_text = "request_page_temprary_not_av";
                    break;
                //7 and 8 , for private                
                case '9':
                    $res_text = "trial_expired";
                    break;
                case '10':
                    $res_text = "numbers_no_notequal_messages";
                    break;
                //11 for private
                case '13':
                    $res_text = "unaccebtable_sender";
                    break;
                case '14':
                    $res_text = "inactive_sender";
                    break;

                case '15':
                    $res_text = "error_to_numbers";
                    break;
                case '16':
                    $res_text = "null_sender_or_error";
                    break;
                case '17':
                    $res_text = "msg_not_encoded_correctly_or_not_entered";
                    break;
                default:
                    $res_text = "unknown_error";
                    break;
            }
            $res['text'] = $res_text;            
            return $res;            
            break;
        default:
            return false;
            break;
    }
}
