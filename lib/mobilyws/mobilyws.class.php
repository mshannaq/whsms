<?php

/**
 * Mobily.ws (SMS Gateway) Class 
 * Done by Mohammed Shannaq
 * www.phpspiders.com 
 * m.shannaq@gmail.com 
 * Class version: 2.2
 * Created Date: 1-may-2007
 * Last Modify: 1-oct-2014 (By fouad mekkey)
 * Modify reason : to be compatibe utf-8
 * 
 * This class comes with the BSD license,
 * http://www.opensource.org/licenses/bsd-license.html
 * 
 * Also you need a vaild account on mobily.ws to send SMS's
 */
class mobilyws {

    /**
     * mobilyws main class function
     *
     * @param string $account_username
     * @param stringe $account_password
     * @return mobilyws
     */
    function mobilyws($account_username, $account_password) {
        $this->username = $account_username;
        $this->password = $account_password;
    }

    /**
     * This function used to get account balance
     *
     * @return Array [connected]: (true,false) [result]: the result
     */
    function get_balance() {
        $url = "http://mobily.ws/api/balance.php?mobile=" . $this->username . "&password=" . $this->password;
        if (!($fp = @fopen($url, "r"))) {
            $rarray['conncted'] = false;
        } else {
            $rarray['connected'] = true;
        }
        $rarray['result'] = @fread($fp, 100);
        //balance will come as 10/7 {account_total:account_current_credit}
        @fclose($fp);
        return $rarray;
    }

    /**
     * This function to send SMS
     *
     * @param string $from (name or number not more that 14 chars)
     * @param string $to (you can use multi numbers as no,otherno,anotherno) and upto 120 mobile number as mobily.ws said, but I advise to use on number every call.
     * @param string $sms_msg (in cp1256 ecnoding format)
     * @return integer
     * (result code => 0: cannot connect to mobily.ws gateway
     *                 1: send ok
     *                 2: cannot sned, the account balance is empty
     *                 3: cannot send, no enough credit
     *                 4: cannot send, incorrect username
     *                 5: cannot send, incorrect password
     * 		       6: cannot send because there are problem , try again
     * 		       13: cannot send due sender username is unaccebtable
     * 		       14: cannot send becaue sender username is inactive and you must activate it before send
     * 		       15: cannot send because one of recipient mobile number is invalid or not standard
     * 		       16: cannot send because sender username is empty or not send with invalid
     *                 17: message text not encodded correctly
     */
    function send_sms($from, $to, $sms_msg) {
        //to avoid \n problem i will convert it to \r
        //$sms_msg = str_replace("\n", '\r', $sms_msg);
        $utf16msg = $this->to_utf16($sms_msg);        
        $to = urlencode($to);       
        //send msg now
        $url = "http://www.mobily.ws/api/msgSend.php?mobile=" . $this->username . "&password=" . $this->password . "&numbers=" . $to . "&sender=" . $from . "&msg=" . $utf16msg . "&dateSend=0&timeSend=0&applicationType=24";
        if (!($fp = @fopen($url, "r"))) {
            $result = "0"; //unable to connect to the url            
        }
        $result = @fread($fp, 10);
        @fclose($fp);
        $result = explode("\r\n", $result);
        $result = $result[(count($result) - 1)];
        return $result;
    }

    /**
     * This Function Converting the given text to UTF-16
     * UTF-16 required when sending SMS
     * 
     * This function support Arabic,English and the special chars only
     * For other language try to move your hands
     * 
     * This is internal function
     * @deprecated 1.0
     * @param string $str
     * @return utf-16_string
     */
    function utf8ToUnicodeCodePoints($str) {

        /*
          if (!mb_check_encoding($str, 'UTF-8')) {
          return false;
          }
         * 
         */

        return preg_replace_callback('/./u', function ($m) {
            $ord = ord($m[0]);
            if ($ord <= 127) {
                return sprintf('%04x', $ord);
            } else {
                return strtoupper(trim(json_encode($m[0]), '"\u'));
            }
        }, $str);
    }
    
    
    function to_utf16($text) {
        $out = "";
        $text = mb_convert_encoding($text, 'UTF-16', 'UTF-8');
        for ($i = 0; $i < mb_strlen($text, 'UTF-16'); $i++)
            $out.= bin2hex(mb_substr($text, $i, 1, 'UTF-16'));
        return $out;
    }

}
