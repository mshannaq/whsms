/* 
 * whsms addon main js file
 * @author: @mshannaq
 */

function unicodeE(strText)
{
    str = "دجحخهعغفقثصضطكمنتالبيسشظزوةىلارؤءئإلإألأآلآ،؛؟ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيـًٌٍَُِّْ";
    for (i = 0; i < strText.length; i++)
        if (str.indexOf(strText.charAt(i)) != -1)
            return 1;
    return 0;
}

$(document).ready(function () {
    var pointscount = jQuery("#pointscount").data("count");
    var charcount = jQuery("#charcount").data("count");

    $("#smsmsg").on('change keyup paste cut input propertychange mousemove ', function () {
        strText = $("#smsmsg").val();
        strLength = strText.length;
        jQuery("#charcount").data("count", strLength);
        jQuery("#charcount").html(strLength);

        var counterI = 0;
        var numChar = strText.length;
        if (unicodeE(strText)) {
            if (numChar > 70)
                while (numChar > 0)
                {
                    numChar -= 67;
                    counterI++;
                }
            else
                counterI++;
            jQuery("#pointscount").data("count", counterI);
            jQuery("#pointscount").html(counterI);
        } else {
            counterI = 0;
            if (numChar > 160)
                while (numChar > 0)
                {
                    numChar -= 153;
                    counterI++;
                }
            else
                counterI++;
            jQuery("#pointscount").data("count", counterI);
            jQuery("#pointscount").html(counterI);
        }


    });


    $("#smsmsg").keypress(function () {





    });


});

function getObj(name)
{
    if (document.getElementById)
        return  document.getElementById(name);
    else if (document.all)
        return document.all[name];
    else if (document.layers)
        return document.layers[name];
}


function getObjI(name, myFrame)
{

    var myFrameObj = getObj(myFrame);
    doc = myFrameObj.contentWindow.document;

    if (doc.getElementById)
        return  doc.getElementById(name);
    else if (doc.all)
        return doc.all[name];
    else if (doc.layers)
        return doc.layers[name];
}





getObj("numChars").value = strLen;
numChar = strLen;
if (unicodeE(getObj("msg").value))
{
    getObj("unicode").value = 1;
    counterI = 0;
    if (numChar > 70)
        while (numChar > 0)
        {
            numChar -= 67;
            counterI++;
        }
    else
        counterI++;
    getObj("numMsgs").value = counterI;
}
else
{
    getObj("unicode").value = 0;
    counterI = 0;
    if (numChar > 160)
        while (numChar > 0)
        {
            numChar -= 153;
            counterI++;
        }
    else
        counterI++;
    getObj("numMsgs").value = counterI;
}



